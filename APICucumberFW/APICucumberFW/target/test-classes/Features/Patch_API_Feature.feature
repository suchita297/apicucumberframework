Feature: Trigger the Patch request with required parameter

@Patch_API
Scenario: Trigger the API request with valid requestbody parameters
Given Replace NAME and JOB in request body
When request with playload
Then  authenticate status code 
And  response body should contain parameters
 
 Scenario Outline: Trigger the API request with valid requestbody parameters
Given Update "<NAME>" and "<JOB>" in request body
When Request with playload
Then Verify status code
And verify response body parameters
 
 Examples:
    |NAME|JOB|
    |SUCHITA|TESTER|
    |ANVIT|DEV|
    |ABHI|BA|	