package stepDefinitions;

import org.testng.Assert;

import EnvironmentandRepository.Environment;
import EnvironmentandRepository.RequestRepository;
import commonmethods.API_Trigger;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Delete_API_stepdefinitions {

	String endpoint;
	Response response;
	ResponseBody responseBody;

	@Given("Enter endpoint for deleteing user")
	public void enter_endpoint_for_deleteing_user() {

		endpoint = Environment.delete_endpoint();
//	    throw new io.cucumber.java.PendingException();
	}
	@When("Send the dalete request with payload")
	public void send_the_dalete_request_with_payload() {
		response = API_Trigger.Delete_API_Trigger(endpoint, endpoint);

	   // throw new io.cucumber.java.PendingException();
	}
	@Then("Validate delete response status code")
	public void validate_delete_response_status_code() {
		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 204);

//	    throw new io.cucumber.java.PendingException();
	}
	@Then("The responsebody should be empty")
	public void the_responsebody_should_be_empty() {
		
//	    throw new io.cucumber.java.PendingException();
	}




}
