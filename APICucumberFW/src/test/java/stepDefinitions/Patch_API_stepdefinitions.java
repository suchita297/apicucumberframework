package stepDefinitions;

import java.time.LocalDateTime;

import org.testng.Assert;

import EnvironmentandRepository.Environment;
import EnvironmentandRepository.RequestRepository;
import commonmethods.API_Trigger;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Patch_API_stepdefinitions {
	String requestbody;
	String endpoint;
	Response response;
	ResponseBody responseBody;
	
	@Given("Replace NAME and JOB in request body")
	public void replace_name_and_job_in_request_body() {
		requestbody = RequestRepository.patch_request_body();
		endpoint = Environment.patch_endpoint();	  
		//throw new io.cucumber.java.PendingException();
	}
	@When("request with playload")
	public void request_with_playload() {
		response = API_Trigger.Patch_API_Trigger(requestbody, endpoint);
				//throw new io.cucumber.java.PendingException();
	}
	@Then("authenticate status code")
	public void authenticate_status_code() {
		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 200, "correct statuscode  and even after retrying for 5 time");

	    //throw new io.cucumber.java.PendingException();
	}
	@Then("response body should contain parameters")
	public void response_body_should_contain_parameters() {
		responseBody = response.getBody();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		// step 3 : parse the requestBody using json path extract the rquestBody
		// parameter
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// step4 : Generate expected Date
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// step 5 : validate using TestNG Assertion

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to name sent in RequestBody");
		Assert.assertEquals(res_job, req_job, "Job in ResposneBody is not equal to job sent in RequestBody");
		Assert.assertEquals(res_updatedAt, expecteddate)
				;

	   // throw new io.cucumber.java.PendingException();
	}




}
