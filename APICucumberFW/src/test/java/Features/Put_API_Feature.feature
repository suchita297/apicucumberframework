Feature: Trigger the Put API with required parameters

@Put_API
Scenario: Trigger the API request with valid requestbody parameters
Given Update NAME and JOB in request body
When Request with playload
Then Verify status code
And verify response body parameters
 
@Put_API
Scenario Outline: Trigger the API request with valid requestbody parameters
Given Update "<NAME>" and "<JOB>" in request body
When Request with playload
Then Verify status code
And verify response body parameters
 
 Examples:
    |NAME|JOB|
    |SUCHITA|TESTER|
    |ANVIT|DEV|
    |ABHI|BA|	